from django.db import models

class Document(models.Model):
	docfile = models.FileField(upload_to='documents')

class Author(models.Model):
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=40)
	email = models.EmailField(blank=True, verbose_name='e-mail')
	def __unicode__(self):
		return u'%s %s' % (self.first_name, self.last_name)

class Book(models.Model):
	book_title = models.CharField(max_length=100)
	authors = models.ForeignKey(Author,verbose_name='author(s)')
	def __unicode__(self):
		return self.book_title
	
class Page(models.Model):
	book = models.ForeignKey(Book)
	number = models.IntegerField()

class Section(models.Model):
	page = models.ForeignKey(Page)
	number = models.IntegerField()

class Image(models.Model):
	section = models.ForeignKey(Section)
	image = models.CharField(max_length=200)
	def __unicode__(self):
		return self.image

class Text(models.Model):
	section = models.ForeignKey(Section)
	text = models.TextField()
	def __unicode__(self):
		return self.text		
	
class ImageText(models.Model):
	section = models.ForeignKey(Section)
	image = models.CharField(max_length=200)
	text = models.TextField()
	def __unicode__(self):
		return u'%s %s' % (self.text, self.image)
